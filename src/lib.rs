#![allow(dead_code, unused_variables, unused_imports)]

use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "oppish")]

struct Opt {
    #[structopt(short = "i", long = "input")]
    input: String,

    #[structopt(name = "FILE", parse(from_os_str))]
    files: Vec<PathBuf>,
}

pub fn run() {
    let opt = Opt::from_args();
    blank_cons(&opt.input);
}

pub fn get_words() {
    // convert &opt.input into
    // a collection of words
}

pub fn blank_cons(s: &str) {
    // This makes me want to
    // make a hangman game.

    let chs = s.chars();
    for c in chs {
        match c {
            'a' => print!("{}", c),
            'e' => print!("{}", c),
            'i' => print!("{}", c),
            'o' => print!("{}", c),
            'u' => print!("{}", c),

            'y' => print!("{}", c),
            ' ' => print!("{}", c),
            _ => print!("_"),
        }
    }
}

pub fn itr_chars(s: &str) -> std::str::CharIndices<'_> {
    // This may or may not be useful 🤔

    let ci = s.char_indices();
    ci
}

pub fn clust() {
    // I have no idea how to do this.
    // Maybe an enum? Rust needs to
    // reject an 's' if it's part of
    // an "sh"...
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn defines_consanant_clusters() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn pushes_ops() {
        assert_eq!(2 + 2, 4);
    }
}
